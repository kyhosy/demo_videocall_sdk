//
//  SceneDelegate.h
//  demo_videocallsdk
//
//  Created by ky ho sy on 9/30/20.
//  Copyright © 2020 ky ho sy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


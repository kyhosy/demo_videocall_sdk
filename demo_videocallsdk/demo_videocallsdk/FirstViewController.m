//
//  FirstViewController.m
//  demo_videocallsdk
//
//  Created by ky ho sy on 9/30/20.
//  Copyright © 2020 ky ho sy. All rights reserved.
//
#import <MytelSupportSDK/MytelSupportSDK-Swift.h>
@import MytelSupportSDK;

#import "FirstViewController.h"


@interface FirstViewController ()

@end

@implementation FirstViewController
static NSString * const sipProxy = @"metfone.mycc.vn";
static NSString * const chatDomain = @"8a43e6d5-b0b2-452d-83a3-013e0c6de22f";
static NSString * const chatScriptUrl = @"https://myccpublic.metfone.com.kh/assets//js/IpccChat.js";
static NSString * const apiBaseUrl = @"https://myccpublic.metfone.com.kh:6210";
static NSString * const accId = @"2d48d112ad36aaaad3b90f510e31f2fd";
static NSString * const hashing_key = @"Metfone1!2@3#";
static NSString * const userName = @"mefone";

//var apiBaseUrl = "https://myccpublic.metfone.com.kh:6210"
//var sipProxy:String = "metfone.mycc.vn"
//var chatDomain = "8a43e6d5-b0b2-452d-83a3-013e0c6de22f"
//var chatScriptUrl = "https://myccpublic.metfone.com.kh/assets//js/IpccChat.js"
//var hashing_key = "Metfone1!2@3#"
//var accId = "2d48d112ad36aaaad3b90f510e31f2fd";
//var userName = "mefone"

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initConfig];
    // Do any additional setup after loading the view.
}

- (void) initConfig{
    [MytelSupportSDK setDefaultSipDomain:sipProxy];
    [MytelSupportSDK setDefaultChat:chatDomain :chatScriptUrl];
    [MytelSupportSDK setDefaultBaseUrl:apiBaseUrl];
    [MytelSupportSDK setAccountConfig:accId :hashing_key ];
}


- (IBAction)on_btn_chat:(id)sender {
    [MytelSupportSDK showChatView:userName];
}
- (IBAction)on_btn_voice:(id)sender {
    [MytelSupportSDK showCallView:TRUE :userName];
}
- (IBAction)on_btn_video:(id)sender {
    [MytelSupportSDK showCallView:FALSE :userName];
}


@end
